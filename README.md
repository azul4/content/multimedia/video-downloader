# video-downloader

GTK application to download videos from websites like YouTube and many others (based on youtube-dl)

https://github.com/Unrud/video-downloader

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/multimedia/video-downloader.git
```
