# Maintainer:  Marcell Meszaros < marcell.meszaros AT runbox.eu >
# Contributor: Kevin Majewski < kevin.majewski02 AT gmail.com >
# Rafael from azul - depends added: gtk4 and libadwaita

pkgname='video-downloader'
pkgver=0.12.5
pkgrel=1
pkgdesc='GTK application to download videos from websites like YouTube and many others (based on yt-dlp)'
arch=('any')
url="https://github.com/Unrud/${pkgname}"
license=('GPL3')
depends=(
  'dconf'
  'gtk3'
  'hicolor-icon-theme'
  'libhandy'
  'python-gobject'
  'yt-dlp'
  'gtk4'
  'libadwaita'
)
makedepends=(
  'librsvg'
  'meson'
)
_tarname="${pkgname}-${pkgver}"
source=("${_tarname}.tar.gz::${url}/archive/refs/tags/v${pkgver}.tar.gz")
b2sums=('07cadc5dc4b5b821f4f6acbfa93c5c3d1b5fbe2f0d8fb2457b6bf6713d69eb193bbdf4369508c769958856467d8d25ac2e9ed9a64b44f2c1dbdfa88630cf047e')

prepare() {
  arch-meson "${_tarname}" 'build'
}

build() {
  meson compile -C 'build'
}

check() {
  meson test -C 'build' --print-errorlogs || warning 'There were test failures'
}

package() {
  # extra depends are needed for yt-dlp - some are mandatory but erroneously set to optional in Arch
  depends+=(
    'ffmpeg'
    'python-brotli'
    'python-mutagen'
    'python-pycryptodomex'
    'python-websockets'
    'python-xattr'
  )

  DESTDIR="${pkgdir}" meson install -C 'build'
}
